const path = require('path');

module.exports = {
  entry:  path.resolve(__dirname, '/src/js/app.js'),
  resolve: {
    alias: {
      js: path.resolve(__dirname, 'src/js/'),
    }
  },
  devtool:'source-map',
  output: {
    path: __dirname + '/dist/js',
    publicPath: '/dist/',
    filename: 'bundle.js'
  },
  module: {
    rules:[ {
      test: /\.js$/,
      exclude: /(node_modules)/,
      use:{
        loader: 'babel-loader'
      }

    }]
  },
  devServer: {
    static: {
      directory: path.join(__dirname, 'dist')
    },
    compress: true,
    port: 9000,
    historyApiFallback: true
  },
}
