export const SLIDING_NAV_SELECTOR = 'header nav';
export const SLIDING_NAV_ANCHOR_SELECTOR = `${SLIDING_NAV_SELECTOR} a`;
export const SLIDING_NAV_LINE_SELECTOR = `${SLIDING_NAV_SELECTOR} .line`;
export const ACTIVE_CLASS_NAME = 'active';
export const MENU_MARGIN = 20;
export const ONE_SECOND = 1000;
