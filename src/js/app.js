import Clock from 'js/Clock';
import SlidingNav from 'js/SlidingNav';
import { debounce } from 'js/utils/utils';
const RESIZE_DELAY = 200;

export default function app() {
  new SlidingNav({ shouldCreateLinks: true});
  new Clock();
}

const debouncedResizeFunction = debounce(function resize() {
  new SlidingNav({ shouldCreateLinks: false});
}, RESIZE_DELAY);

addEventListener('DOMContentLoaded', function () {
  app();
});
addEventListener('resize', debouncedResizeFunction);
