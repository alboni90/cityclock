import { ONE_SECOND } from 'js/utils/constants';
import getLocaleData from 'js/getLocaleData/index';
let clockPlaceholder;

export default class Clock {
  constructor() {
    clockPlaceholder = document.querySelector('#clock');
    setInterval(this.clockUpdate, ONE_SECOND);
  }

  clockUpdate = () => {
    window.requestAnimationFrame(() => {
      clockPlaceholder.textContent = `City time ⏰ ${this.getCityTime()}`;
    });
  }

  getCityTime = () => {
    const time = new Date();
    const cityLocaleData = getLocaleData();
    return time.toLocaleTimeString(cityLocaleData.locale, {
      timeZone: cityLocaleData.timezone
    });
  }
}


