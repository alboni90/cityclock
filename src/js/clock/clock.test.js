import getLocaleData from '../getLocaleData/index';
import { HTML } from '../../files/utils/testData';
import Clock from './index';

const jsdom = require('jsdom');
const { JSDOM } = jsdom;

jest.mock('../getLocaleData/index', () => jest.fn().mockImplementation(() => ({
  locale: 'es-CR',
  timezone: 'America/Costa_Rica'
})));


describe('Clock', function () {
  jest.useFakeTimers();

  beforeEach(() => {
    new JSDOM(HTML);
  });

  it('should call setInterval with clockUpdate function', function () {
    //Arrange
    jest.spyOn(global, 'setInterval');
    //Act
    const clock = new Clock();
    //Assert
    expect(setInterval).toHaveBeenCalledWith(clock.clockUpdate, 1000);
  });

  it('should select the #clock element', function () {
    //Arrange
    jest.spyOn(global.document, 'querySelector');
    //Act
    new Clock();
    //Assert
    expect(global.document.querySelector).toHaveBeenCalledWith('#clock');
  });

  describe('clockUpdate', function () {
    it('should call requestAnimationFrame', function () {
      //Arrange
      const clock = new Clock();
      jest.spyOn(global, 'requestAnimationFrame');
      //Act
      clock.clockUpdate();
      //Assert
      expect(requestAnimationFrame).toHaveBeenCalled();
    })
  });

  describe('getCityTime', function () {
    it('it should call getLocaleData', function () {
      // arrange
      const clock = new Clock();

      // act
      clock.getCityTime();

      //assert
      expect(getLocaleData).toHaveBeenCalled()

    })
  });
});