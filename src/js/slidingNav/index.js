import {
  SLIDING_NAV_ANCHOR_SELECTOR,
  ACTIVE_CLASS_NAME,
  SLIDING_NAV_SELECTOR,
  SLIDING_NAV_LINE_SELECTOR,
  MENU_MARGIN
} from 'js/utils/constants';
import navigationData from '../../files/navigation.json';
export default class SlidingNav {
  constructor({ shouldCreateLinks }) {
    if (shouldCreateLinks) {
      const navElement = document.querySelector(SLIDING_NAV_SELECTOR);
      this.createCityLinks(navElement);
    }
    this.initSlidingNav();
    
    this.initSlidingNav = this.initSlidingNav.bind(this);
    this.createCityLinks = this.createCityLinks.bind(this);
    this.setActiveLink = this.setActiveLink.bind(this);
    this.moveLineElement = this.moveLineElement.bind(this);
    this.removeActiveClassFromAllLinks = this.removeActiveClassFromAllLinks.bind(this);
    this.getCityLinks = this.getCityLinks.bind(this);
    this.getLineElement = this.getLineElement.bind(this);
  }

  initSlidingNav() {
    const locationHash = location.hash;
    const activeLink = locationHash && this.setActiveLink(locationHash);
    activeLink && this.moveLineElement(activeLink);
  }

  createCityLinks(navElement) {
    const { cities } = navigationData;
    cities.forEach((city) => {
      const anchor = document.createElement('a');

      anchor.href = `#/${city.section}`;
      anchor.textContent = city.label;
      anchor.addEventListener('click', (event) => {
        const selectedLink = event.target;
        this.removeActiveClassFromAllLinks();
        this.setActiveLink(selectedLink.hash);
        this.moveLineElement(selectedLink)
      });

      navElement.appendChild(anchor);
    });

    navElement.setAttribute('style', `--items: ${cities.length};`);
  }

  setActiveLink(locationHash) {
    const linkToActivate = document.querySelector(`${SLIDING_NAV_ANCHOR_SELECTOR}[href='${locationHash}']`);
    linkToActivate.classList.add(ACTIVE_CLASS_NAME);
    return linkToActivate;
  }

  moveLineElement(selectedLink) {
    const lineElement = this.getLineElement();
    const { left, width } = selectedLink.getBoundingClientRect();
    lineElement.setAttribute('style', `
    left: ${Math.trunc(left) - MENU_MARGIN}px;
    width: ${Math.trunc(width)}px;
    opacity:1;`);
  }

  removeActiveClassFromAllLinks() {
    const cityLinks = this.getCityLinks();
    cityLinks.forEach((cityLink) => {
      cityLink.classList.remove(ACTIVE_CLASS_NAME);
    })
  }

  getCityLinks() {
    return document.querySelectorAll(SLIDING_NAV_ANCHOR_SELECTOR) || [];
  }

  getLineElement() {
    return document.querySelector(SLIDING_NAV_LINE_SELECTOR);
  }

}
