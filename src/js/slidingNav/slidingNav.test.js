import { HTML_WITOUT_LINKS } from '../../files/utils/testData';
import SlidingNav from './index';
// jest.mock('./index');

const jsdom = require('jsdom');
const { JSDOM } = jsdom;

describe('slidingNav', function () {
  beforeEach(() => {
    new JSDOM(HTML_WITOUT_LINKS);
  });
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should call createCityLinks if shouldCreateLinks is true', function () {
    //arrange
    jest.spyOn(SlidingNav.prototype, 'createCityLinks');
    jest.spyOn(SlidingNav.prototype, 'initSlidingNav');

    //act
    new SlidingNav({ shouldCreateLinks: true });
    //assert
    expect(SlidingNav.prototype.createCityLinks).toHaveBeenCalled();
  });

  // it('should not call createCityLinks if shouldCreateLinks is false', function () {
  //   //arrange
  //   jest.spyOn(SlidingNav.prototype, 'createCityLinks');
  //   jest.spyOn(SlidingNav.prototype, 'initSlidingNav');

  //   //act
  //   new SlidingNav({ shouldCreateLinks: false });
  //   //assert
  //   expect(SlidingNav.createCityLinks).not.toHaveBeenCalled();
  // });

  // it('should call initSlidingNav always', function () {
  //   //arrange 
  //   jest.spyOn(SlidingNav.prototype, 'initSlidingNav');

  //   //act
  //   new SlidingNav({ shouldCreateLinks: false });
  //   //assert
  //   expect(SlidingNav.initSlidingNav).toHaveBeenCalled();
  // });


  // it('should display as many items as cities given on json param', function () {
  //   //arrange
  //   //act
  //   //assert
  // });

  // describe('when a city is clicked the line span', function () {

  //   it('should become visible', function () {

  //   });

  //   it('should update its left and width props to match the item clicked bottom border', function () {

  //   });

  //   it('should update location.hash', function () {

  //   });

  //   it('should set the clicked link to .active', function () {

  //   });
  // });
});