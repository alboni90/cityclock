
import Clock from './Clock';
import app from './app';
import SlidingNav from './SlidingNav'
import { HTML_WITOUT_LINKS } from '../files/utils/testData';

const jsdom = require('jsdom');
const { JSDOM } = jsdom;

jest.mock('./Clock/index');
jest.mock('./SlidingNav/index');

describe('app', function () {

  it('should init the clock', function () {
    //act
    app();
    //assert
    expect(Clock).toHaveBeenCalled();
  });

  it('should init the slidingNav', function () {
    //act
    app();
    //assert
    expect(SlidingNav).toHaveBeenCalled();
    
  });
});