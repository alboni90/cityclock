const timezones = {
  'cupertino': {
    timezone: 'PST8PDT',
    locale: 'en-US'
  },
  'new-york-city': {
    timezone: 'America/New_York',
    locale: 'en-US'
  },
  'london': {
    timezone: 'Europe/London',
    locale: 'en-GB'
  },
  'amsterdam': {
    timezone: 'Europe/Amsterdam',
    locale: 'nl-NL'
  },
  'tokyo': {
    timezone: 'Asia/Tokyo',
    locale: 'ja'
  },
  'hong-kong': {
    timezone: 'Asia/Hong_Kong',
    locale: 'zh-HK'
  },
  'sydney': {
    timezone: 'Australia/Sydney',
    locale: 'en-AU'
  },
  clientLocale: {
    locale: navigator.languages && navigator.languages.length
      ? navigator.languages[0]
      : navigator.language,
    timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
  }
}

export default function getLocaleData() {
  const cityHash = location.hash;
  const city = cityHash.replace('#/', '');
  return timezones[city] || timezones.clientLocale;
}