export const HTML = `<!DOCTYPE html>
<html lang="en">
<head>
</head>

<body id="root">
  <header>
    <nav style="--items: 7;">
      <span class="line"></span>
      <a href="#/cupertino">Cupertino</a>
      <a href="#/new-york-city" class="">New York City</a>
      <a href="#/london" class="">London</a>
      <a href="#/amsterdam" class="">Amsterdam</a>
      <a href="#/tokyo" class="">Tokyo</a>
      <a href="#/hong-kong" class="">Hong Kong</a>
      <a href="#/sydney" class="active">Sydney</a>
    </nav>
  </header>
  <section>
    <h1 id="clock"></h1>
  </section>
</body>
</html>`;

export const HTML_WITOUT_LINKS = `<!DOCTYPE html>
<html lang="en">
<head>
</head>

<body id="root">
  <header>
    <nav>
      <span class="line"></span>
    </nav>
  </header>
  <section>
    <h1 id="clock"></h1>
  </section>
</body>
</html>`;