# Critical Mass assessment
Welcome this is Luis Bonilla's implementation for Critical Mass assessment

# Setup
We need to install a node version manager in order to make easy to move through versions depending of what we are developing.


## Windows setup
_Before proceed to install any node version please check this out:_
Since nvm is a only Macos/Linux project we need to use another tool for windows, please refer to the next [link](https://github.com/coreybutler/nvm-windows).
In order to use the proper node version for your OS architecture [go here](https://github.com/coreybutler/nvm-windows#usage)

## MacOS or Linux setup
If you don't have nvm installed please refer to [this link](https://github.com/nvm-sh/nvm#installing-and-updating).

Once you got nvm installed, go to cmd and type `nvm install 16.17.0` that will install the needed node and npm version. 
If you've installed more than one node's version make sure your current version is 16.17.0 or just type `nvm use 16.17.0`. 

## Setting up the project
1. Clone the project.
2. Go to cmd then to the project's dir.
3. Run `npm install`
4. For development mode refer to next Section.
5. Run `npm run build:prod`
6. Go open the `dist` folder on the Finder and double click on the `index.html` file.

The solution is 100% frontend so there is no need for a server.

## Development
For review Development enviroment:

1. Go to cmd then to the project's dir.
2. Run `npm run build:dev` for single dev mode build or `npm run build:watch` for watch mode
3. On a different cmd tab run `npm run start:dev`, this will get the webpack-dev-server up.

# Folder structure
  * dist: contains full fledge css and html, also will contain the bundle after webpack runs.
  * node_modules: any installed module.
  * src: 
    * files: contains navigation files.
    * js: contains ES6 modules grouped up by functionality and the corresponding tests.
    * tests: contaings a setup file for the jest.


# About the process

This project could be done without any bundling tools, however, it was added webpack + babel in order to take advantage of bable preset optimizations for safari (as it was required on the instructions). Also, there was added Jest to support unit testing.

## Responsiveness
  The menu and the clock were meant to need as less CSS media query rules as possible so those are only used to resize the navigation font. Anything else should flow.

## JS Classes
  Initially the approach was merely based on js functions and modules to overcome the needed tasks, although, the approach was not friendly for jest testing so the Clock and the SlidingNav where refactored into classes so those could be mocked up.

## Tradeoffs
  * Adjusting css and/or animations `onresize` could be very expencive operation so it was `debounced` to keep it cheaper.
  * The clock uses requestAnimationFrame so it doesn't mess with the main thread, initially it was just called recursively but it was also expensive so it was decided to use a `setInterval` to call the `clockUpdate` function every second. It was lost smoothness and we gained performance.
  * When `Clock` and `SlidingNav` were refactored into Classes those became heavier and slower than the previous approach, however, it was kept that way for testability sake.

## Other considerations
  There was included some unit tests, some of them are working, some other are not. 
  The not working or commented unit test were kept only for showing the process sake. Under different conditions commented or not working unit tests should't be pushed.
  
  * jsconfig.json file was added for helping jest find some modules. And to make it easier for devs (aka me) navigate throughout files by doing (cmd + click)
  * the packages installed were mostly used for testing, building, optimizing code. As you can see all those are on the `devDependencies` section, the solution was delivered using only css and es6 as it was required.
  * it cannot be assumed that navigation file should be served through an endpoint since as per the requirements it was not clear how often it would change, I asked but it was said to me that it was up to me. So for perf sake it is injected on the bundle.


